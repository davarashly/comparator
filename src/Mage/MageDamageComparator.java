package Mage;

import java.util.Comparator;

public class MageDamageComparator implements Comparator<Mage> {
    public int compare(Mage o1, Mage o2) {
        if (o1.getDamage() > o2.getDamage())
            return 1;
        else if (o1.getDamage() < o2.getDamage())
            return -1;
        else
            return 0;
    }
}
