package Mage;

import java.util.NavigableSet;
import java.util.TreeSet;

import static Features.Funcs.*;

public class TopMages {
    TreeSet<Mage> mages;

    public TopMages(TreeSet<Mage> mages) {
        this.mages = mages;
    }

    public void showTop(int a) {
        NavigableSet<Mage> topMages = mages.descendingSet();
        int i = 0;
        System.out.println("\n\nТоп " + word(a, new String[]{"маг", "мага", "магов"}) + ":\n");
        for (Mage mage : topMages) {
            if (i < a) {
                mage.printMage();
                i++;
            }
        }
    }
}
