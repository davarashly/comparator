import Mage.*;
import Warrior.*;
import Knight.*;

import java.util.Comparator;
import java.util.TreeSet;

import static Features.TextColors.*;

public class Main {
    static void separate() {
        System.out.print(ANSI_PURPLE);

        for (int i = 0; i < 30; i++) {
            System.out.print("=");
        }

        System.out.print(ANSI_RESET);
    }

    public static void main(String[] args) {
        /* ----------------------- WARRIORS ----------------------- */
        System.out.println(ANSI_RED + "~~~~~~ Warriors! ~~~~~~" + ANSI_RESET);

        Comparator<Warrior> warriorDamageComparator = new WarriorDamageComparator();
        Comparator<Warrior> warriorHPComparator = new WarriorHPComparator();
        TreeSet<Warrior> warriors = new TreeSet<>(warriorDamageComparator.thenComparing(warriorHPComparator));

        int warriorsQuantity = 20; // Введите количество войнов


        for (int i = 0; i < warriorsQuantity; i++) {
            warriors.add(new Warrior());
        }

        for (Warrior warrior : warriors) {
            warrior.printWarrior();
        }

        separate();

        System.out.println("\n\nСамый слабый войн:\n" + warriors.first());
        System.out.println("Самый сильный войн:\n" + warriors.last());

        /* ----------------------- MAGES ----------------------- */
        System.out.println(ANSI_RED + "~~~~~~ Mages! ~~~~~~" + ANSI_RESET);

        Comparator<Mage> mageComparator = new MageDamageComparator();
        TreeSet<Mage> mages = new TreeSet<>(mageComparator);

        int magesQuantity = 20; // Введите количество магов

        for (int i = 0; i < magesQuantity; i++) {
            mages.add(new Mage());
        }

        TopMages topMages = new TopMages(mages);

        for (Mage mage : mages) {
            mage.printMage();
        }

        separate();

        topMages.showTop(10); // Топ сколько?

        /* ----------------------- Knights ----------------------- */
        System.out.println(ANSI_RED + "~~~~~~ Knights! ~~~~~~" + ANSI_RESET);

        Comparator<Knight> knightComparator = new KnightComparator();

        TreeSet<Knight> knights = new TreeSet<Knight>(knightComparator);

        int knightsQuantity = 20; // Введите количество рыцарей

        for (int i = 0; i < knightsQuantity; i++) {
            knights.add(new Knight());
        }

        for (Knight knight : knights) {
            knight.printKnight();
        }

        separate();

        System.out.println("\n\nСамый сильный рыцарь:\n" + knights.last());

    }
}
