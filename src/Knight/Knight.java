package Knight;

import Warrior.Warrior;

import java.util.Random;

import static Features.Funcs.*;

public class Knight extends Warrior {
    private double armor;

    static int armMax = 500;
    static int armMin = 100;

    public Knight() {
        super();
        Random rnd = new Random();
        armor = round(armMin + (armMax - armMin) * rnd.nextDouble(), 2);
    }

    public Knight(String name, double hp, double damage, double armor) {
        super(name, hp, damage);
        if (armor > armMax)
            this.armor = round(armMax, 2);
        else
            this.armor = round(armor, 2);
    }

    public void printKnight() {
        System.out.println(this);
    }

    public String toString() {
        return "" +
                "Name   :    " + this.getName() + "\n" +
                "HP     :    " + this.getHp() + "\n" +
                "Damage :    " + this.getDamage() + "\n" +
                "Armor  :    " + this.armor + "\n";
    }


    public double getArmor() {
        return armor;
    }

    public void setArmor(double armor) {
        this.armor = armor;
    }
}
