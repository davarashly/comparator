package Knight;

import java.util.Comparator;

public class KnightComparator implements Comparator<Knight> {
    public int compare(Knight o1, Knight o2) {
        int p1 = 0, p2 = 0;

        if (o1.getHp() > o2.getHp())
            p1 += Math.abs(o1.getHp() - o2.getHp()) > 15 ? 2 : 1;
        else if (o1.getHp() < o2.getHp())
            p2 += Math.abs(o1.getHp() - o2.getHp()) > 15 ? 2 : 1;


        if (o1.getDamage() > o2.getDamage())
            p1 += Math.abs(o1.getDamage() - o2.getDamage()) > 10 ? 3 : 2;
        else if (o1.getDamage() < o2.getDamage())
            p2 += Math.abs(o1.getDamage() - o2.getDamage()) > 10 ? 3 : 2;


        if (o1.getArmor() > o2.getArmor())
            p1 += Math.abs(o1.getArmor() - o2.getArmor()) > 100 ? Math.abs(o1.getArmor() - o2.getArmor()) > 200 ? 6 : 5 : 3;
        else if (o1.getArmor() < o2.getArmor())
            p2 += Math.abs(o1.getArmor() - o2.getArmor()) > 100 ? Math.abs(o1.getArmor() - o2.getArmor()) > 200 ? 6 : 5 : 3;


        if (p1 > p2)
            return 1;
        else if (p1 < p2)
            return -1;
        else
            return 0;
    }
}
