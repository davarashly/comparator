package Utils;

import java.util.Random;

public class NameGenerator {
    String[] names = {"The Giantslayer", "The Merciful", "The Rabid", "The Dread", "The Axe",
            "Shadowsworn", "Stone Hand", "Swift Mantle", "Silentclaw", "Grandbolt", "The Silencer",
            "The Martyr", "The Cobra", "The Vermin", "The Feisty", "Bonetalon", "Stormeye", "Proudcrest",
            "Demontongue", "Demonblade", "The Feisty", "The Rabid", "The Mutant", "The Hallowed", "The Thug",
            "Rockstride", "Proudgaze", "Ragepelt", "Bright Snarl", "Deadbow", "The Bear", "The Marked", "The Animal",
            "The Menace", "The Blade", "Giant Grim", "Gorehide", "Stonerage", "Blood Heart", "Swifthand"};

    public String getName() {
        Random rnd = new Random();
        return names[rnd.nextInt(names.length)];
    }
}