package Warrior;

import Utils.NameGenerator;

import static Features.Funcs.*;

import java.util.Random;

public class Warrior {
    private String name;
    private double hp;
    private double damage;

    static int hpMax = 100;
    static int dmgMax = 30;
    static int dmgMin = 10;

    public Warrior() {
        Random rnd = new Random();
        name = new NameGenerator().getName();
        hp = round(90 + (100 - 90) * rnd.nextDouble(), 2);
        damage = round(dmgMin + (dmgMax - dmgMin) * rnd.nextDouble(), 2);
    }

    public Warrior(String name, double hp, double damage) {
        this.name = name;
        if (hp > hpMax)
            this.hp = round(hpMax, 2);
        else
            this.hp = round(hp, 2);
        if (damage > dmgMax)
            this.damage = round(dmgMax, 2);
        else
            this.damage = round(damage, 2);
    }

    public void printWarrior() {
        System.out.println(this);
    }

    public String toString() {
        return "" +
                "Name   :    " + this.name + "\n" +
                "HP     :    " + this.hp + "\n" +
                "Damage :    " + this.damage + "\n";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }
}