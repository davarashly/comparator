package Warrior;

import java.util.Comparator;

public class WarriorDamageComparator implements Comparator<Warrior> {
    public int compare(Warrior o1, Warrior o2) {
        if (o1.getDamage() > o2.getDamage())
            return 1;
        else if (o1.getDamage() < o2.getDamage())
            return -1;
        else
            return 0;
    }
}
