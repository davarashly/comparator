package Warrior;

import java.util.Comparator;

public class WarriorHPComparator implements Comparator<Warrior> {
    public int compare(Warrior o1, Warrior o2) {
        if (o1.getHp() > o2.getHp())
            return 1;
        else if (o1.getHp() < o2.getHp())
            return -1;
        else
            return 0;
    }
}
